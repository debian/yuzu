Source: yuzu
Section: games
Priority: optional
Maintainer: Andrea Pappacoda <tachi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-build-api (= 1),
Build-Depends-Arch:
 catch2 (>= 2.13.7) <!nocheck>,
 cmake (>= 3.15),
 gamemode-dev [linux-any],
 git,
 glslang-tools,
 libavcodec-dev,
 libavfilter-dev,
 libavutil-dev,
 libboost-context-dev,
 libboost-dev (>= 1.79),
 libcpp-httplib-dev,
 libcpp-jwt-dev,
 libcubeb-dev,
 libdynarmic-dev (>= 6.7.0),
 libenet-dev,
 libfmt-dev (>= 9.0.0),
 liblz4-dev (>= 1.8),
 libmbedtls-dev,
 libmicroprofile-dev,
 liboaknut-dev [any-arm64],
 libopus-dev,
 libsdl2-dev,
 libsimpleini-dev,
 libstb-dev,
 libswscale-dev,
 libusb-1.0-0-dev,
 libva-dev,
 libvulkan-dev (>= 1.3.246),
 libvulkan-memory-allocator-dev,
 libxbyak-dev [any-amd64],
 libzstd-dev (>= 1.5),
 llvm-dev,
 ninja-build,
 nlohmann-json3-dev (>= 3.8),
 pkg-config,
 qt6-base-dev,
 qt6-base-private-dev,
 qt6-multimedia-dev,
 qt6-tools-dev,
 qt6-webengine-dev,
 spirv-headers,
 tzdata,
 tzdata-legacy,
 vulkan-utility-libraries-dev,
 zlib1g-dev (>= 1.2),
Standards-Version: 4.7.0
Homepage: https://yuzu-emu.org
Vcs-Git: https://salsa.debian.org/debian/yuzu.git
Vcs-Browser: https://salsa.debian.org/debian/yuzu
Rules-Requires-Root: no
X-Style: black

Package: yuzu
Architecture: any-amd64 any-arm64
Depends:
 sse4.2-support [any-amd64],
 ${misc:Depends},
 ${shlibs:Depends},
Description: Nintendo Switch Emulator
 yuzu is an experimental open-source emulator for the Nintendo Switch
 from the creators of Citra.
 .
 It is written in C++ with portability in mind, with builds actively
 maintained for Windows and Linux.
